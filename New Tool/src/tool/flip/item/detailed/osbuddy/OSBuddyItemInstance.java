package tool.flip.item.detailed.osbuddy;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stuart on 4/24/2017.
 */

//{"overall":9568,"buying":9572,"buyingQuantity":10860,"selling":9565,"sellingQuantity":16856}

public class OSBuddyItemInstance {

    public OSBuddyItemInstance(int overall, int buying, int buyingQuantity, int selling, int sellingQuantity) {
        this.overall = overall;
        this.buying = buying;
        this.buyingQuantity = buyingQuantity;
        this.selling = selling;
        this.sellingQuantity = sellingQuantity;
    }

    public int getOverall() {
        return overall;
    }

    public int getBuying() {
        return buying;
    }

    public int getBuyingQuantity() {
        return buyingQuantity;
    }

    public int getSelling() {
        return selling;
    }

    public int getSellingQuantity() {
        return sellingQuantity;
    }

    @SerializedName("overall")
    private int overall;
    @SerializedName("buying")
    private int buying;
    @SerializedName("buyingQuantity")
    private int buyingQuantity;
    @SerializedName("selling")
    private int selling;
    @SerializedName("sellingQuantity")
    private int sellingQuantity;

}
