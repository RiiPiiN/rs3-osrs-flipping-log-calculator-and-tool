package tool.flip.item.detailed;

/**
 * Created by Stuart on 4/24/2017.
 */
public final class ItemInstanceConstants {

    public final static String RUNESCAPE_ITEM_INSTANCE_BASE_LINK = "http://services.runescape.com/m=itemdb_oldschool";

    public final static String RUNESCAPE_ITEM_INSTANCE_DETAILS = "/api/catalogue/detail.json?item=";

    public final static String RUNESCAPE_ITEM_INSTANCE_GRAPH_DETAILS = "/api/graph/";

    public final static String getRunescapeItemInstanceDetails(int id) {
        return RUNESCAPE_ITEM_INSTANCE_BASE_LINK + RUNESCAPE_ITEM_INSTANCE_DETAILS + id;
    }

    /**
     *
     *
     * @param id
     * @return Epoch Time : Value
     */
    public final static String getRunescapeItemInstanceGraphDetails(int id) {
        return RUNESCAPE_ITEM_INSTANCE_BASE_LINK + RUNESCAPE_ITEM_INSTANCE_GRAPH_DETAILS + id + ".json";
    }

    public final static String OSBUDDY_ITEM_INSTANCE_BASE_LINK = "http://api.rsbuddy.com/grandExchange?a=guidePrice&i=";

    public final static String getOsbuddyItemInstanceDetails(String id) {
        return OSBUDDY_ITEM_INSTANCE_BASE_LINK + id;
    }

}
