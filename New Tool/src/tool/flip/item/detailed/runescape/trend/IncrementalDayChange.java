package tool.flip.item.detailed.runescape.trend;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stuart on 4/24/2017.
 */
public class IncrementalDayChange {

    public String getTrend() {
        return trend;
    }

    public String getChange() {
        return change;
    }

    @SerializedName("trend")
    private String trend;
    @SerializedName("change")
    private String change;

    public IncrementalDayChange(String trend, String change) {
        this.trend = trend;
        this.change = change;
    }

}
