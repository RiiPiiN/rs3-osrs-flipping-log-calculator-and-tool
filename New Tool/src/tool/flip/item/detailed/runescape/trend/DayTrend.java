package tool.flip.item.detailed.runescape.trend;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stuart on 4/24/2017.
 */
public class DayTrend {

    public String getTrend() {
        return trend;
    }

    public String getPrice() {
        return price;
    }

    @SerializedName("trend")
    private String trend;
    @SerializedName("price")
    private String price;

    public DayTrend(String trend, String price) {
        this.trend = trend;
        this.price = price;
    }

}
