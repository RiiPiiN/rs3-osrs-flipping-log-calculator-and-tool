package tool.flip.item.detailed.runescape.graph;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stuart on 4/25/2017.
 */
public class RunescapeGraphInstance {

    public Map<String, Integer> getDailyTrend() {
        return dailyTrend;
    }

    public Map<String, Integer> getAverageTrend() {
        return averageTrend;
    }

    @SerializedName("daily")
    private Map<String, Integer> dailyTrend;
    @SerializedName("average")
    private Map<String, Integer> averageTrend;

    public RunescapeGraphInstance(HashMap<String, Integer> dailyTrend, HashMap<String, Integer> averageTrend) {
        this.dailyTrend = dailyTrend;
        this.averageTrend = averageTrend;
    }
}
