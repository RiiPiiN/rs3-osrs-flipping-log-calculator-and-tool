package tool.flip.item.detailed.runescape;

import com.google.gson.annotations.SerializedName;
import tool.flip.item.detailed.runescape.trend.DayTrend;
import tool.flip.item.detailed.runescape.trend.IncrementalDayChange;

/**
 * Created by Stuart on 4/24/2017.
 */

//{"item":{"icon":"http://services.runescape.com/m=itemdb_oldschool/1493029836302_obj_sprite.gif?id=13263",
//        "icon_large":"http://services.runescape.com/m=itemdb_oldschool/1493029836302_obj_big.gif?id=13263","id":13263,"type":"Default",
//        "typeIcon":"http://www.runescape.com/img/categories/Default",
//        "name":"Abyssal bludgeon","description":"Elements of deceased Abyssal Sires have been fused together.",
//        "current":{"trend":"neutral","price":"40.9m"},"today":{"trend":"negative","price":"- 35.9k"},"members":"true",
//        "day30":{"trend":"positive","change":"+12.0%"},"day90":{"trend":"positive","change":"+6.0%"},
//        "day180":{"trend":"negative","change":"-10.0%"}}}

public final class RunescapeItemInstance {

    public RunescapeItemInstance(String icon, String largeIcon, int id, String type, String typeIcon, String name, String description, DayTrend currentTrend, DayTrend todaysTrend, boolean membersItem, IncrementalDayChange thirtyDayTrend, IncrementalDayChange ninetyDayTrend, IncrementalDayChange oneHundredEigthyDayTrend) {
        this.icon = icon;
        this.largeIcon = largeIcon;
        this.id = id;
        this.type = type;
        this.typeIcon = typeIcon;
        this.name = name;
        this.description = description;
        this.currentTrend = currentTrend;
        this.todaysTrend = todaysTrend;
        this.membersItem = membersItem;
        this.thirtyDayTrend = thirtyDayTrend;
        this.ninetyDayTrend = ninetyDayTrend;
        this.oneHundredEigthyDayTrend = oneHundredEigthyDayTrend;
    }

    public String getIcon() {
        return icon;
    }

    public String getLargeIcon() {
        return largeIcon;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getTypeIcon() {
        return typeIcon;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public DayTrend getCurrentTrend() {
        return currentTrend;
    }

    public DayTrend getTodaysTrend() {
        return todaysTrend;
    }

    public boolean isMembersItem() {
        return membersItem;
    }

    public IncrementalDayChange getThirtyDayTrend() {
        return thirtyDayTrend;
    }

    public IncrementalDayChange getNinetyDayTrend() {
        return ninetyDayTrend;
    }

    public IncrementalDayChange getOneHundredEigthyDayTrend() {
        return oneHundredEigthyDayTrend;
    }

    @SerializedName("icon")
    private String icon;
    @SerializedName("icon_large")
    private String largeIcon;
    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private String type;
    @SerializedName("typeIcon")
    private String typeIcon;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("current")
    private DayTrend currentTrend;
    @SerializedName("today")
    private DayTrend todaysTrend;
    @SerializedName("members")
    private boolean membersItem;
    @SerializedName("day30")
    private IncrementalDayChange thirtyDayTrend;
    @SerializedName("day90")
    private IncrementalDayChange ninetyDayTrend;
    @SerializedName("day180")
    private IncrementalDayChange oneHundredEigthyDayTrend;

}
