package tool.flip.item.osbuddy;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stuart on 4/19/2017.
 *
 * {"2": {"name": "Cannonball", "buy_average": 203, "id": 2, "overall_average": 204, "members": true, "sp": 5, "sell_average": 204},
 * "6": {"name": "Cannon base", "buy_average": 186300, "id": 6, "overall_average": 194000, "members": true, "sp": 187500, "sell_average": 187583},
 *
 * "12": {"sp": 187500, "overall_average": 215000, "members": true, "sell_average": 190748, "name": "Cannon furnace", "id": 12, "buy_average": 186706},
 * "12510": {"id": 12510, "sp": 6000, "overall_average": 0, "name": "Armadyl chaps", "buy_average": 106817, "sell_average": 106817, "members": true},
 */
public final class OSBuddyInstance {

    @SerializedName("name")
    private final String name;
    @SerializedName("buy_average")
    private final int buyAverage;
    @SerializedName("id")
    private final int id;
    @SerializedName("overall_average")
    private final int overallAverage;
    @SerializedName("members")
    private final boolean members;
    @SerializedName("sp")
    private final int storePrice;
    @SerializedName("sell_average")
    private final int sellAverage;

    public OSBuddyInstance(String name, int buyAverage, int id, int overallAverage, boolean members, int storePrice, int sellAverage) {
        this.name = name;
        this.buyAverage = buyAverage;
        this.id = id;
        this.overallAverage = overallAverage;
        this.members = members;
        this.storePrice = storePrice;
        this.sellAverage = sellAverage;
    }

    public int getBuyAverage() {
        return buyAverage;
    }

    public int getId() {
        return id;
    }

    public int getOverallAverage() {
        return overallAverage;
    }

    public int getSellAverage() {
        return sellAverage;
    }

    public int getStorePrice() {
        return storePrice;
    }

    public String getName() {
        return name;
    }

    public boolean isMembers() {
        return members;
    }

}
