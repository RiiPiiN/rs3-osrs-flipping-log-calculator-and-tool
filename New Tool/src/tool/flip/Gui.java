package tool.flip;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.*;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import java.text.DateFormat;
import java.text.NumberFormat;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import tool.flip.entry.EntryInstance;
import tool.flip.entry.table.AbstractCalculationTable;
import tool.flip.entry.table.AbstractEntryTable;
import tool.flip.entry.table.edit.EditEntry;

import tool.flip.item.detailed.ItemInstanceConstants;
import tool.flip.item.detailed.runescape.RunescapeItemInstance;
import tool.flip.item.detailed.runescape.graph.RunescapeGraphInstance;
import tool.flip.item.osbuddy.OSBuddyInstance;
import tool.flip.item.osbuddy.table.AbstractOSBuddyItemsTable;

@SuppressWarnings("serial")
public class Gui extends JFrame {

	private JPanel pane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	public JTable table;

	private JCheckBox checkBox = new JCheckBox("Enable logging");;
	
	private AbstractEntryTable entryTable;
	private AbstractCalculationTable calculationTable;
	private AbstractOSBuddyItemsTable itemTable;

	
	public AbstractCalculationTable getCalculationTable() {
		return calculationTable;
	}

	private ArrayList<EntryInstance> entries;
	private HashMap<Integer, OSBuddyInstance> items;
	
	private int rowAtPoint;
	private int rowAtPoint_1;

	private JPanel jpanel_1;

	private RunescapeGraphInstance runescapeGraph;

	private RunescapeItemInstance runescapeItem;
	private String runescapeItemPath;

	private JsonParser parser = new JsonParser();
	private JsonElement object;

	private BufferedImage image;
	private JLabel picLabel;
	private JLabel nameLabel;
	private JLabel idLabel;
	private JLabel descriptionLabel;
	private JLabel membersLabel;
	private JLabel currentTrendLabel;
	private JLabel todaysTrendLabel;
	private JLabel thirtyDayTrendLabel;
	private JLabel ninetyDayTrendLabel;
	private JLabel oneHundredEightyDayTrendLabel;
	private JLabel typeLabel;


	/**
	 * Lets launch the application here.
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Builds the UI of the Application and handles most of the processes.
	 * @throws IOException 
	 * @throws JsonSyntaxException 
	 * @throws JsonIOException 
	 */
	public Gui() throws JsonIOException, JsonSyntaxException, IOException {

		/*
		 * Sets the UI theme of the application.
		 */
		try {
			UIManager.setLookAndFeel("com.jtattoo.plaf.graphite.GraphiteLookAndFeel");
		} catch (ClassNotFoundException | UnsupportedLookAndFeelException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}

		/*
		 * Set the size of the Application.
		 */
		this.setBounds(100, 100, 800, 560);

		/*
		 * Set the title of the application.
		 */
		this.setTitle("RiiPiiNFtW's Flipping Log and Profit Calculator | Pre-Alpha V.01");

		/*
		 * Set the default layout for the application.
		 * @return null = AbsoluteLayout (Coordinates)
		 */
		this.getContentPane().setLayout(null);

		/*
		 * Can the application be resized manually?
		 */
		this.setResizable(false);

		/*
		 * Will the application terminate after being closed?
		 */
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		/*
		 * Instancing a JPanel
		 * @param border
		 * 		new EmptyBorder(Top: 5, Left: 5, Bottom: 5, Right: 5)
		 * @param layout
		 * 		new BorderLayout(HGap: 0, VGap: 0)
		 */
		pane = new JPanel();
		pane.setBorder(new EmptyBorder(5, 5, 5, 5));
		pane.setLayout(new BorderLayout(0, 0));

		/*
		 * Set {@link pane} as the main Gui panel.
		 */
		this.setContentPane(pane);

		/*
		 * Instance a new tabbed pane at the top of {@link pane}.
		 */
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);

		/*
		 * Adds the new {@link tabbedPane} to {@link pane}
		 */
		pane.add(tabbedPane, BorderLayout.CENTER);

		/*
		 * Instances a new JPanel
		 */
		JPanel tablePanel = new JPanel();

		/*
		 * Adds {@link tablePanel} to {@tabbedPane}
		 */
		tabbedPane.addTab("Logs and Calculations", null, tablePanel, null);

		/*
		 * Sets the layout for {@link tablePanel}
		 * @param layout
		 * 		@return null = AbsoluteLayout(X, Y)
		 */
		tablePanel.setLayout(null);

		/*
		 * Loads data from a .json file into an ArrayList instanced by EntryInstance
		 */
		Gson gson = new Gson();
		ArrayList<EntryInstance> _entries = gson.fromJson(Files.newBufferedReader(Paths.get("data", "items.json")), new TypeToken<List<EntryInstance>>(){}.getType());

		/*
		 * Set our global ArrayList to the local one loaded from the .json file.
		 */
		this.entries = _entries;

		/*
		 * Instances a horizontal sparator on the UI.
		 * @param backgroundColor
		 * 		@return BLACK
		 * @param bounds
		 * 		@return X: 10, Y: 218, Width: 639, Height: 2
		 */
		JSeparator horizontalSeparator = new JSeparator();
		horizontalSeparator.setForeground(Color.BLACK);
		horizontalSeparator.setBounds(10, 218, 760, 2);

		/*
		 * Adds the horizontal separator to the @Gui.
		 */
		tablePanel.add(horizontalSeparator);

		/*
		 * Instanced a vertical separator on the UI.
		 * @param backgroundColor
		 * 		@return BLACK
		 * @param orientation
		 * 		@return VERTICAL (Default = Horizontal)
		 * @param bounds
		 * 		@return X: 324, Y: 221, Width: 2, Height 267
		 */
		JSeparator verticalSeparator = new JSeparator();
		verticalSeparator.setForeground(Color.BLACK);
		verticalSeparator.setOrientation(SwingConstants.VERTICAL);
		verticalSeparator.setBounds(390, 221, 2, 267);

		/*
		 * Adds the vertical separator to the @Gui.
		 */
		tablePanel.add(verticalSeparator);

		/*
		 * Instances a JLabel
		 * @param font
		 * 		@return Tahoma 16 | Plain
		 * @param bounds
		 * 		@return X: 77, Y: 225, Width: 174, Height: 18
		 */
		JLabel calculateProfitLabel = new JLabel("Calculate Profit Margin");
		calculateProfitLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		calculateProfitLabel.setBounds(117, 225, 174, 18);

		/*
		 * Adds the JLabel to the @Gui.
		 */
		tablePanel.add(calculateProfitLabel);

		/*
		 * Instances a JLabel
		 * @param font
		 * 		@return Tahome 16 | Plain
		 * @param bounds
		 * 		@return bounds X: 444, Y: 222, Width: 179, Height: 20
		 */
		JLabel logEntryLabel = new JLabel("Log Entry");
		logEntryLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		logEntryLabel.setBounds(554, 222, 179, 20);

		/*
		 * Adds the JLabel to the @Gui.
		 */
		tablePanel.add(logEntryLabel);
		
		JLabel lblBuyingPrice = new JLabel("Buying Price");
		lblBuyingPrice.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblBuyingPrice.setBounds(50, 250, 79, 14);
		tablePanel.add(lblBuyingPrice);

		textField = new JTextField();
		textField.setBounds(60, 269, 132, 20);
		tablePanel.add(textField);
		textField.setColumns(10);
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar() ;

				if (!Character.isDigit(c)) {
					e.consume() ;
				}
			}
		});
		
		JLabel lblSellingPrice = new JLabel("Selling Price");
		lblSellingPrice.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSellingPrice.setBounds(50, 300, 79, 20);
		tablePanel.add(lblSellingPrice);
		
		textField_1 = new JTextField();
		textField_1.setBounds(60, 331, 132, 20);
		tablePanel.add(textField_1);
		textField_1.setColumns(10);
		textField_1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar() ;

				if (!Character.isDigit(c)) {
					e.consume() ;
				}
			}
		});
		
		JLabel lblAmount = new JLabel("Amount");
		lblAmount.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAmount.setBounds(50, 362, 105, 14);
		tablePanel.add(lblAmount);
		
		textField_2 = new JTextField();
		textField_2.setBounds(60, 387, 132, 20);
		tablePanel.add(textField_2);
		textField_2.setColumns(10);
		textField_2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar() ;

				if (!Character.isDigit(c)) {
					e.consume() ;
				}
			}
		});
		
		JButton button = new JButton("Calculate Margin");
		button.setFont(new Font("Tahoma", Font.BOLD, 13));
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (textField.toString() != null || textField.toString() != null) {
					if (!textField.getText().toString().matches("\\d+")) {
						JOptionPane.showMessageDialog(null, "Buying Price Input contains invalid characters!",
								"An Error Occured!", 1, null);
						return;
					}
					if (!textField_1.getText().toString().matches("\\d+")) {
						JOptionPane.showMessageDialog(null, "Selling Price Input contains invalid characters!",
								"An Error Occured!", 1, null);
						return;
					}
				}

				int buyInput = Integer.parseInt(textField.getText().toString());
				int sellInput = Integer.parseInt(textField_1.getText().toString());
				int amountInput = textField_2.getText().toString().equals("") ? 1
						: Integer.parseInt(textField_2.getText().toString());

				if (amountInput > 1) {
					textField_3.setText(String.valueOf(
							NumberFormat.getInstance().format(Math.subtractExact(buyInput, sellInput) * amountInput)));
				} else {
					textField_3.setText(
							String.valueOf(NumberFormat.getInstance().format(Math.subtractExact(buyInput, sellInput))));
				}
			}
			
		});
		button.setBounds(50, 423, 142, 22);
		tablePanel.add(button);
		
		
		
		JButton button_1 = new JButton("Clear");
		button_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		button_1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				textField.setText("");
				textField_1.setText("");
				textField_2.setText("");
				textField_3.setText("");
			}
			
		});
		button_1.setBounds(198, 423, 160, 22);
		tablePanel.add(button_1);
		
		textField_3 = new JTextField();
		textField_3.setEditable(false);
		textField_3.setBounds(222, 331, 132, 20);
		tablePanel.add(textField_3);
		textField_3.setColumns(10);

		
		JLabel lblPotentialProfitMade = new JLabel("Potential Profit Made");
		lblPotentialProfitMade.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblPotentialProfitMade.setBounds(211, 304, 143, 14);
		tablePanel.add(lblPotentialProfitMade);
		
		JLabel lblNewLabel_2 = new JLabel("Item Name");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_2.setBounds(456, 251, 105, 14);
		tablePanel.add(lblNewLabel_2);
		
		textField_4 = new JTextField();
		textField_4.setBounds(466, 271, 277, 20);
		tablePanel.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Bought For");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_3.setBounds(456, 301, 92, 20);
		tablePanel.add(lblNewLabel_3);
		
		textField_5 = new JTextField();
		textField_5.setBounds(466, 321, 277, 20);
		tablePanel.add(textField_5);
		textField_5.setColumns(10);
		textField_5.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar() ;

				if (!Character.isDigit(c)) {
					e.consume() ;
				}
			}
		});
		
		JLabel lblSoldFor = new JLabel("Sold For");
		lblSoldFor.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSoldFor.setBounds(456, 351, 105, 14);
		tablePanel.add(lblSoldFor);
		
		textField_6 = new JTextField();
		textField_6.setBounds(466, 371, 277, 20);
		tablePanel.add(textField_6);
		textField_6.setColumns(10);
		textField_6.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar() ;

				if (!Character.isDigit(c)) {
					e.consume() ;
				}
			}
		});
		
		JLabel lblAmountSold = new JLabel("Amount Sold");
		lblAmountSold.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAmountSold.setBounds(456, 401, 105, 14);
		tablePanel.add(lblAmountSold);
		
		textField_7 = new JTextField();
		textField_7.setBounds(466, 421, 277, 20);
		tablePanel.add(textField_7);
		textField_7.setColumns(10);
		textField_7.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar() ;

				if (!Character.isDigit(c)) {
					e.consume() ;
				}
			}
		});

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();

		JButton button_2 = new JButton("Insert to Table");
		button_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				entries.add(new EntryInstance(dateFormat.format(date), textField_4.getText(), Integer.parseInt(textField_5.getText()),
						textField_6.getText().equals("") ? 1 : Integer.parseInt(textField_6.getText().toString()), 
								Integer.parseInt(textField_7.getText())));
				Gui.this.saveAll();
				textField_4.setText("");
				textField_5.setText("");
				textField_6.setText("");
				textField_7.setText("");
			}
		});
		button_2.setBounds(552, 447, 130, 22);
		tablePanel.add(button_2);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 760, 144);
		scrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		tablePanel.add(scrollPane);
		
		table = new JTable();
		table.setFont(new Font("Tahoma", Font.PLAIN, 11));
		table.setBorder(new MatteBorder(1, 1, 1, 1, new Color(0, 0, 0)));
		entryTable = new AbstractEntryTable(this.entries);
		table = new JTable(entryTable) {
			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
				Component comp = super.prepareRenderer(renderer, row, column);
				Object value = getModel().getValueAt(row, column);
				comp.setForeground(Color.WHITE);
				return comp;
			}
		};
		table.setBackground(Color.BLACK);
		table.getTableHeader().setReorderingAllowed(false);
		table.setColumnSelectionAllowed(false);
		table.getTableHeader().setResizingAllowed(false);
		table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

		table.setModel(entryTable);
		
		final JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem editItem = new JMenuItem("Edit Entry", new Icon() {
			@Override
			public void paintIcon(Component c, Graphics g, int x, int y) {

			}

			@Override
			public int getIconWidth() {
				return 0;
			}

			@Override
			public int getIconHeight() {
				return 0;
			}
		});
		JMenuItem deleteItem = new JMenuItem("Delete Entry");
		JMenuItem clearList = new JMenuItem("Clear Table");

		popupMenu.add(editItem);
		popupMenu.add(deleteItem);
		popupMenu.add(clearList);

		popupMenu.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						rowAtPoint = table
								.rowAtPoint(SwingUtilities.convertPoint(popupMenu, new Point(0, 0), table));
						if (rowAtPoint > -1) {
							table.setRowSelectionInterval(rowAtPoint, rowAtPoint);
						}
					}
				});
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {
				
			}
		});

		editItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							Object itemName = entryTable.getValueAt(table.getSelectedRow(), 1);
							Object boughtAmount = entryTable.getValueAt(table.getSelectedRow(), 2);
							Object soldAmount = entryTable.getValueAt(table.getSelectedRow(), 3);
							Object itemAmount = entryTable.getValueAt(table.getSelectedRow(), 4);
							EditEntry edit = new EditEntry(entryTable, table, Gui.this);
							edit.textField.setText(itemName.toString());
							edit.textField_1.setText(Gui.unformatNumber(boughtAmount.toString()));
							edit.textField_2.setText(Gui.unformatNumber(soldAmount.toString()));
							edit.textField_3.setText(Gui.unformatNumber(itemAmount.toString()));

							edit.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
							edit.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}

		});

		deleteItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (rowAtPoint > -1) {
					entries.remove(rowAtPoint);
				}

					Gui.this.saveAll();

			}
		});
		
		clearList.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				int selectedOption = JOptionPane.showConfirmDialog(null, 
                        "By deleting the table, you will be\n" +
                        "irreversably deleting all data and \n" + 
                        "will not be recoverable.", 
                        "Are you sure?", 
                        JOptionPane.YES_NO_OPTION); 
				if (selectedOption == JOptionPane.YES_OPTION) {
					entries.clear();
					Gui.this.saveAll();
				}

			}
		});
		
		table.setComponentPopupMenu(popupMenu);
		table.setModel(entryTable);

		table.getColumnModel().getColumn(0).setPreferredWidth(85);
		table.getColumnModel().getColumn(1).setPreferredWidth(62);
		table.getColumnModel().getColumn(2).setPreferredWidth(70);
		table.getColumnModel().getColumn(3).setPreferredWidth(70);
		table.getColumnModel().getColumn(4).setPreferredWidth(70);
		table.getColumnModel().getColumn(5).setPreferredWidth(90);
		scrollPane.setViewportView(table);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane_1.setBounds(10, 160, 760, 52);
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		tablePanel.add(scrollPane_1);
		calculationTable = new AbstractCalculationTable(this.entries);
		JTable table_1 = new JTable(calculationTable) {
			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
				Component comp = super.prepareRenderer(renderer, row, column);
				Object value = getModel().getValueAt(row, column);
				comp.setForeground(Color.WHITE);
				return comp;
			}
		};
		
		table_1.setBackground(Color.BLACK);
		table_1.getTableHeader().setReorderingAllowed(false);
		table_1.getTableHeader().setResizingAllowed(false);
		table_1.setRowSelectionAllowed(false);
		table_1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		
		table_1.setModel(calculationTable);

		table_1.getColumnModel().getColumn(0).setPreferredWidth(99);
		table_1.getColumnModel().getColumn(1).setPreferredWidth(120);
		table_1.getColumnModel().getColumn(2).setPreferredWidth(127);
		table_1.getColumnModel().getColumn(3).setPreferredWidth(93);
		table_1.getColumnModel().getColumn(4).setPreferredWidth(81);
		table_1.setRowHeight(22);
		scrollPane_1.setViewportView(table_1);

		/*
		 * Instances a new JPanel
		 */
		JPanel graphPanel = new JPanel();

		graphPanel.setLayout(null);

		/*
		 * Adds {@link tablePanel} to {@tabbedPane}
		 */
		//tabbedPane.addTab("Graphs", null, graphPanel, null);

		Gson gson_3 = new Gson();
		this.items = gson_3.fromJson(new BufferedReader(new InputStreamReader(new URL("https://rsbuddy.com/exchange/summary.json").openStream())), new TypeToken<HashMap<Integer, OSBuddyInstance>>(){}.getType());

		/*
		 * Instances a new JPanel
		 */
		JPanel osbuddyItems = new JPanel();

		osbuddyItems.setLayout(null);

		/*
		 * Adds {@link tablePanel} to {@tabbedPane}
		 */
		tabbedPane.addTab("OSBuddy Items", null, osbuddyItems, null);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane_2.setBounds(5, 5, 778, 450);
		osbuddyItems.add(scrollPane_2);
		itemTable = new AbstractOSBuddyItemsTable(this.items) {};
		JTable table_2 = new JTable(itemTable) {
			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
				Component comp = super.prepareRenderer(renderer, row, column);
				Object value = getModel().getValueAt(row, column);
				comp.setForeground(Color.WHITE);
				if (column == 7) {
					if (String.valueOf(value).contains("(Not Accurate!)")) {
						comp.setForeground(Color.YELLOW);
				} else if (Integer.parseInt(Gui.unformatNumber(String.valueOf(value))) > 0) {
					comp.setForeground(Color.GREEN);
					} else if (Integer.parseInt(Gui.unformatNumber(String.valueOf(value))) < 0) {
					comp.setForeground(Color.RED);
					} else {
						comp.setForeground(Color.WHITE);
					}
				} else {
					comp.setForeground(Color.WHITE);
				}
				return comp;
			}
		};

		table_2.setCellSelectionEnabled(true);
		table_2.getTableHeader().setReorderingAllowed(false);
		table_2.getTableHeader().setUpdateTableInRealTime(true);
		table_2.getTableHeader().setResizingAllowed(true);
		table_2.setBackground(Color.BLACK);

		table_2.setModel(itemTable);

		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		table.getColumnModel().getColumn(6).setCellRenderer(rightRenderer);

		table_2.getColumnModel().getColumn(0).setPreferredWidth(55);
		table_2.getColumnModel().getColumn(1).setPreferredWidth(110);
		table_2.getColumnModel().getColumn(2).setPreferredWidth(75);
		table_2.getColumnModel().getColumn(3).setPreferredWidth(75);
		table_2.getColumnModel().getColumn(4).setPreferredWidth(75);
		table_2.getColumnModel().getColumn(5).setPreferredWidth(70);
		table_2.getColumnModel().getColumn(6).setPreferredWidth(55);
		table_2.setRowHeight(22);
		scrollPane_2.setViewportView(table_2);

		final JPopupMenu popupMenu_1 = new JPopupMenu();

		JMenuItem searchJagex = new JMenuItem("Runescape Details");

		popupMenu_1.add(searchJagex);

		popupMenu_1.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						rowAtPoint_1 = table_2
								.rowAtPoint(SwingUtilities.convertPoint(popupMenu_1, new Point(0, 0), table_2));
						if (rowAtPoint_1 > -1) {
							table_2.setRowSelectionInterval(rowAtPoint_1, rowAtPoint_1);
						}
					}
				});
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {

			}
		});

		searchJagex.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if (picLabel != null || nameLabel != null || idLabel != null || descriptionLabel != null || membersLabel != null
									|| currentTrendLabel != null || todaysTrendLabel != null || thirtyDayTrendLabel != null || ninetyDayTrendLabel != null
									|| oneHundredEightyDayTrendLabel != null || typeLabel != null) {
								jpanel_1.remove(picLabel);
								jpanel_1.remove(nameLabel);
								jpanel_1.remove(idLabel);
								jpanel_1.remove(descriptionLabel);
								jpanel_1.remove(membersLabel);
								jpanel_1.remove(typeLabel);
								jpanel_1.remove(currentTrendLabel);
								jpanel_1.remove(todaysTrendLabel);
								jpanel_1.remove(thirtyDayTrendLabel);
								jpanel_1.remove(ninetyDayTrendLabel);
								jpanel_1.remove(oneHundredEightyDayTrendLabel);
							}
							object = parser.parse(new BufferedReader(new InputStreamReader(new URL(ItemInstanceConstants.getRunescapeItemInstanceDetails(Integer.parseInt(Gui.unformatNumber(String.valueOf(itemTable.getValueAt(rowAtPoint_1, 0)))))).openStream())));
							runescapeItem = gson_3.fromJson(object.getAsJsonObject().get("item"), RunescapeItemInstance.class);

							object = parser.parse(new BufferedReader(new InputStreamReader(new URL(ItemInstanceConstants.getRunescapeItemInstanceGraphDetails(Integer.parseInt(Gui.unformatNumber(String.valueOf(itemTable.getValueAt(rowAtPoint_1, 0)))))).openStream())));
							runescapeGraph = gson_3.fromJson(object, RunescapeGraphInstance.class);

							image = ImageIO.read(new URL(runescapeItem.getLargeIcon()));
							picLabel = new JLabel(new ImageIcon(image));
							jpanel_1.add(picLabel).setBounds(5, 5, 96, 96);

							nameLabel = new JLabel(runescapeItem.getName());
							nameLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
							jpanel_1.add(nameLabel).setBounds(195, 5, nameLabel.getText().length() * 13, 20);

							idLabel = new JLabel(String.valueOf(runescapeItem.getId()));
							idLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
							jpanel_1.add(idLabel).setBounds(40, 106, nameLabel.getText().length()*13, 20);

							descriptionLabel = new JLabel(runescapeItem.getDescription());
							descriptionLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
							jpanel_1.add(descriptionLabel).setBounds(232, 30, descriptionLabel.getText().length() * 13, 20);

							membersLabel = new JLabel(String.valueOf(runescapeItem.isMembersItem()));
							membersLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
							jpanel_1.add(membersLabel).setBounds(252, 55, membersLabel.getText().length() * 13, 20);

							typeLabel = new JLabel(runescapeItem.getType());
							typeLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
							jpanel_1.add(typeLabel).setBounds(190, 80, typeLabel.getText().length() * 13, 20);

							currentTrendLabel = new JLabel(runescapeItem.getCurrentTrend().getTrend() + ", " + runescapeItem.getCurrentTrend().getPrice());
							currentTrendLabel.setForeground(runescapeItem.getCurrentTrend().getTrend().contains("negative") ? Color.RED :
									runescapeItem.getCurrentTrend().getTrend().contains("positive") ? Color.GREEN : Color.BLACK);
							currentTrendLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
							jpanel_1.add(currentTrendLabel).setBounds(120, 150, currentTrendLabel.getText().length() * 16, 20);

							todaysTrendLabel = new JLabel(runescapeItem.getTodaysTrend().getTrend() + ", " + runescapeItem.getTodaysTrend().getPrice());
							todaysTrendLabel.setForeground(runescapeItem.getTodaysTrend().getTrend().contains("negative") ? Color.RED :
									runescapeItem.getTodaysTrend().getTrend().contains("positive") ? Color.GREEN : Color.BLACK);
							todaysTrendLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
							jpanel_1.add(todaysTrendLabel).setBounds(510, 150, todaysTrendLabel.getText().length() * 16, 20);

							thirtyDayTrendLabel = new JLabel(runescapeItem.getThirtyDayTrend().getTrend() + ", " + runescapeItem.getThirtyDayTrend().getChange());
							thirtyDayTrendLabel.setForeground(runescapeItem.getThirtyDayTrend().getTrend().contains("negative") ? Color.RED :
									runescapeItem.getThirtyDayTrend().getTrend().contains("positive") ? Color.GREEN : Color.BLACK);
							thirtyDayTrendLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
							jpanel_1.add(thirtyDayTrendLabel).setBounds(60, 235, thirtyDayTrendLabel.getText().length() * 16, 20);

							ninetyDayTrendLabel = new JLabel(runescapeItem.getNinetyDayTrend().getTrend() + ", " + runescapeItem.getNinetyDayTrend().getChange());
							ninetyDayTrendLabel.setForeground(runescapeItem.getNinetyDayTrend().getTrend().contains("negative") ? Color.RED :
									runescapeItem.getNinetyDayTrend().getTrend().contains("positive") ? Color.GREEN : Color.BLACK);
							ninetyDayTrendLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
							jpanel_1.add(ninetyDayTrendLabel).setBounds(310, 235, ninetyDayTrendLabel.getText().length() * 16, 20);

							oneHundredEightyDayTrendLabel = new JLabel(runescapeItem.getOneHundredEigthyDayTrend().getTrend() + ", " + runescapeItem.getOneHundredEigthyDayTrend().getChange());
							oneHundredEightyDayTrendLabel.setForeground(runescapeItem.getOneHundredEigthyDayTrend().getTrend().contains("negative") ? Color.RED :
									runescapeItem.getOneHundredEigthyDayTrend().getTrend().contains("positive") ? Color.GREEN : Color.BLACK);
							oneHundredEightyDayTrendLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
							jpanel_1.add(oneHundredEightyDayTrendLabel).setBounds(550, 235, oneHundredEightyDayTrendLabel.getText().length() * 16, 20);

							jpanel_1.repaint();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				});
			}

		});

		table_2.setComponentPopupMenu(popupMenu_1);

		textField_8 = new JTextField();
		textField_8.setBounds(5, 462, 100, 20);

		osbuddyItems.add(textField_8);

		textField_8.setColumns(10);

		checkBox = new JCheckBox("Enable logging");
		checkBox.setText("Balls");
		checkBox.setToolTipText("Select to search for potential profits under the value of your parameters.");
		checkBox.setBounds(110, 462, 20, 20);

		osbuddyItems.add(checkBox);

		JComboBox<String> searchParameters = new JComboBox<String>(new String[] { "Item ID", "Item Name", "Potential Profit"  });
		searchParameters.setBounds(140, 462, 120, 20);
		osbuddyItems.add(searchParameters);

		List<OSBuddyInstance> searchIndex = new ArrayList<OSBuddyInstance>();

		AbstractOSBuddyItemsTable searchTable;

		JButton button_3 = new JButton("Search");
		button_3.setFont(new Font("Tahoma", Font.BOLD, 13));
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (searchParameters.getSelectedItem().equals("Item ID")) {
					if (!textField_8.getText().toString().matches("\\d+")) {
						JOptionPane.showMessageDialog(null, "You cannot search for Items via IDs while having letters within the search parameters!",
								"An Error Occured!", 1, null);
						return;
					}
					for (OSBuddyInstance item : items.values()) {
						if (item.getId() == Integer.parseInt(textField_8.getText())) {
							searchIndex.add(item);
						}
					}
					itemTable = new AbstractOSBuddyItemsTable(searchIndex);
					table_2.setModel(itemTable);
				}
				if (searchParameters.getSelectedItem().equals("Item Name")) {
					for (OSBuddyInstance item : items.values()) {
						if (item.getName().toLowerCase().contains(textField_8.getText().toLowerCase())) {
							searchIndex.add(item);
						}
					}
					itemTable = new AbstractOSBuddyItemsTable(searchIndex);
					table_2.setModel(itemTable);
				}
				if (searchParameters.getSelectedItem().equals("Potential Profit"))
				if (checkBox.isSelected()) {
					if (!textField_8.getText().toString().matches("\\d+")) {
						JOptionPane.showMessageDialog(null, "You cannot search for items via potential while having letters within the search parameters!",
								"An Error Occured!", 1, null);
						return;
					}
					for (OSBuddyInstance item : items.values()) {
						if (Math.subtractExact(item.getSellAverage(), item.getBuyAverage()) < Integer.parseInt(textField_8.getText())) {
							searchIndex.add(item);
						}
					}
					itemTable = new AbstractOSBuddyItemsTable(searchIndex);
					table_2.setModel(itemTable);
				} else {
					if (!textField_8.getText().toString().matches("\\d+")) {
						JOptionPane.showMessageDialog(null, "You cannot search for prices via potential while having letters within the search parameters!",
								"An Error Occured!", 1, null);
						return;
					}
					for (OSBuddyInstance item : items.values()) {
						if (Math.subtractExact(item.getSellAverage(), item.getBuyAverage()) > Integer.parseInt(textField_8.getText())) {
							searchIndex.add(item);
						}
					}
					itemTable = new AbstractOSBuddyItemsTable(searchIndex);
					table_2.setModel(itemTable);
				}
				searchIndex.clear();
				itemTable.fireTableDataChanged();
			}
		});
		button_3.setBounds(270, 462, 130, 20);
		osbuddyItems.add(button_3);

		JButton button_4 = new JButton("Main Table");
		button_4.setFont(new Font("Tahoma", Font.BOLD, 13));
		button_4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				itemTable = new AbstractOSBuddyItemsTable(items);
				table_2.setModel(itemTable);
				itemTable.fireTableDataChanged();
			}
		});

		button_4.setBounds(410, 462, 130, 20);
		osbuddyItems.add(button_4);

		JButton button_5 = new JButton("Refresh Table");
		button_5.setFont(new Font("Tahoma", Font.BOLD, 13));
		button_5.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					items = gson_3.fromJson(new BufferedReader(new InputStreamReader(new URL("https://rsbuddy.com/exchange/summary.json").openStream())), new TypeToken<HashMap<Integer, OSBuddyInstance>>(){}.getType());
					itemTable = new AbstractOSBuddyItemsTable(items);
					table_2.setModel(itemTable);
					itemTable.fireTableDataChanged();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});

		button_5.setBounds(650, 462, 130, 20);
		osbuddyItems.add(button_5);

		JPanel itemPanel = new JPanel();

		itemPanel.setLayout(null);

		/*
		 * Adds {@code tablePanel} to {@tabbedPane}
		 */
		tabbedPane.addTab("Item Panel", null, itemPanel, null);

		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane_3.setBounds(5, 5, 778, 470);
		scrollPane_3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		itemPanel.add(scrollPane_3);

		jpanel_1 = new JPanel();
		jpanel_1.setLayout(null);
		jpanel_1.setBounds(0, 0, scrollPane_3.getWidth(), 600);
		scrollPane_3.getViewport().add(jpanel_1);

		JLabel idLabel = new JLabel("ID:");
		idLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		idLabel.setBounds(10, 106, idLabel.getText().length() * 10, 20);
		jpanel_1.add(idLabel);

		JLabel nameLabel = new JLabel("Name:");
		nameLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		nameLabel.setBounds(150, 5, 50, 20);
		jpanel_1.add(nameLabel);

		JLabel descriptionLabel = new JLabel("Description:");
		descriptionLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		descriptionLabel.setBounds(150, 30, descriptionLabel.getText().length() * 10, 20);
		jpanel_1.add(descriptionLabel);

		JLabel membersLabel = new JLabel("Members Item:");
		membersLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		membersLabel.setBounds(150, 55, membersLabel.getText().length() * 10, 20);
		jpanel_1.add(membersLabel);

		JLabel typeLabel = new JLabel("Type:");
		typeLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		typeLabel.setBounds(150, 80, typeLabel.getText().length() * 10, 20);
		jpanel_1.add(typeLabel);

		JLabel currentTrendLabel = new JLabel("Current Trend:");
		currentTrendLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		currentTrendLabel.setBounds(120, 130, currentTrendLabel.getText().length() * 16, 20);
		jpanel_1.add(currentTrendLabel);

		JLabel todaysTrendLabel = new JLabel("Today's Trend:");
		todaysTrendLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		todaysTrendLabel.setBounds(510, 130, todaysTrendLabel.getText().length() * 16, 20);
		jpanel_1.add(todaysTrendLabel);

		JLabel thirtyDayTrendLabel = new JLabel("30 Day Trend:");
		thirtyDayTrendLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		thirtyDayTrendLabel.setBounds(60, 210, thirtyDayTrendLabel.getText().length() * 16, 20);
		jpanel_1.add(thirtyDayTrendLabel);

		JLabel ninetyDayTrendLabel = new JLabel("90 Day Trend:");
		ninetyDayTrendLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		ninetyDayTrendLabel.setBounds(310, 210, ninetyDayTrendLabel.getText().length() * 16, 20);
		jpanel_1.add(ninetyDayTrendLabel);

		JLabel oneHundredEightyDayTrendLabel = new JLabel("180 Day Trend:");
		oneHundredEightyDayTrendLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		oneHundredEightyDayTrendLabel.setBounds(550, 210, oneHundredEightyDayTrendLabel.getText().length() * 16, 20);
		jpanel_1.add(oneHundredEightyDayTrendLabel);

	}
	
	public void saveAll() {
		try {
			this.save(this);
			this.reload(this);
			entryTable.fireTableDataChanged();
			calculationTable.fireTableDataChanged();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void save(Gui gui) throws IOException {
		Gson gson = new Gson();
		Files.write(Paths.get("data", "items.json"), gson.toJson(gui.entries).getBytes(StandardCharsets.UTF_8),
				StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
	}
	
	public void reload(Gui gui) throws IOException {
		Gson gson = new Gson();
		ArrayList<EntryInstance> entries = gson.fromJson(Files.newBufferedReader(Paths.get("data", "items.json")), new TypeToken<List<EntryInstance>>(){}.getType());
		gui.entries.clear();
		gui.entries.addAll(entries);
		entries.clear();
	}

	public static String formatNumber(double number) {
		return NumberFormat.getInstance().format(number);
	}

	public static String unformatNumber(String number) {
		return number.replaceAll(",","");
	}
	
}
