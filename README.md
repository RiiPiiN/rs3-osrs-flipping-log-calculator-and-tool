# README #

This program was put together as more of a learning experience for myself. After successfully implementing the basic functions of the first panel were completed on my first tool, I decided to redo the design and add more functionality to the program. At this point, what it does is far beyond my initial goals, but I had more plans for this.

### What is this tool used for? ###

This tool is going to be used for OSRS players who flip a lot or even just occasionally to track their transactions and grab more detailed information on prices from both OSBuddy's API and OSRS' GE API. With the information of both API's, users will have a better grasp on the behavior of prices for an item.

### How do I get set up? ###

Just pull it and set it up in an IDE. Any will work, you just have to put the effort in to use it.

### Contribution guidelines ###

After two months of having this as a private repository, and another month of inactivity with it, I decided it'd be put to better use releasing it to the public to improve it where I can't.

While everyone is available to pull and write, you must write to a new branch where I can approve each branch and merge it to the branch. Each completed branch will be put through testing and review to prevent bugs or malicious tampering. This program does not request any personal details!

### Who do I talk to? ###

* Stuart Perera - Owner and Writer