package osrs.merch.entry;

public class EntryInstance {
	
	public String name;
	public int bought;
	public int sold;
	public int amount;
	
	public EntryInstance(String name, int bought, int sold, int amount) {
		super();
		this.name = name;
		this.bought = bought;
		this.sold = sold;
		this.amount = amount;
	}
	
	public String getName() {
		return name;
	}

	public int getBought() {
		return bought;
	}

	public int getSold() {
		return sold;
	}

	public int getAmount() {
		return amount;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setBought(int bought) {
		this.bought = bought;
	}

	public void setSold(int sold) {
		this.sold = sold;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
}
