package osrs.merch.entry.table;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.plaf.synth.SynthSeparatorUI;
import javax.swing.table.AbstractTableModel;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import osrs.merch.Gui;

import osrs.merch.entry.EntryInstance;

public class EntryAbstractTable extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private ArrayList<EntryInstance> entryData;
    private String[] columnNames =  {"Item Name", "Price Bought",
			"Price Sold", "Amount", "Individual Profit Margin", "Total Margin"};

    boolean[] columnEditables = new boolean[] { false, false, false, false, false, false };

	public boolean isCellEditable(int row, int column) {
		return columnEditables[column];
	}

    public EntryAbstractTable(ArrayList<EntryInstance> entries) {
        this.entryData = entries;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return entryData.size();
    }
    
    @Override
    public void setValueAt(Object object, int row, int column) {
    	try {
			EntryInstance entryObject = entryData.get(row);
			if(entryObject == null) return;
	        switch(column) {
	            case 0:
	            	entryObject.setName(object.toString());
	            	entryData.get(row).setName(object.toString());
	            	break;
	            case 1:
	            	entryObject.setBought((int) object);
	            	entryData.get(row).setBought((int) object);
	            	break;
	            case 2:
	            	entryObject.setSold((int) object);
	            	entryData.get(row).setSold((int) object);
	            	break;
	            case 3:
	            	entryObject.setAmount((int) object);
	            	entryData.get(row).setAmount((int) object);
	            	break;
	            default:
	            	break;
	        }
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		} catch (JsonIOException e) {
			e.printStackTrace();
		}
    }

    @Override
    public Object getValueAt(int row, int column) {
        Object entryAttribute = null;
        EntryInstance entryInstance = entryData.get(row);
        if(entryInstance==null)return null;
        switch(column) {
            case 0: 
            	entryAttribute = entryInstance.getName();
            	break;
            case 1:
            	entryAttribute = Gui.formatNumber((int) entryInstance.getBought());
            	break;
            case 2:
            	entryAttribute = Gui.formatNumber((int) entryInstance.getSold());
            	break;
            case 3:
            	entryAttribute = Gui.formatNumber((int) entryInstance.getAmount());
            	break;
            case 4:
            	entryAttribute = Gui.formatNumber((int) Math.subtractExact(entryInstance.getSold(), entryInstance.getBought()));
            	break;
            case 5:
            	entryAttribute = Gui.formatNumber((int) Math.subtractExact(entryInstance.getSold(), entryInstance.getBought()) * entryInstance.getAmount());
            	break;
            default:
            	break;
        }
        return entryAttribute;
    }
    
    public void removeEntry(int entry) {
    	entryData.remove(entry);
    	fireTableDataChanged();
    }
}