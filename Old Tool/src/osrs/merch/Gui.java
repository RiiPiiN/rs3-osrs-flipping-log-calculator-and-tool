package osrs.merch;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.NumberFormat;
import java.util.*;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import osrs.merch.entry.EntryInstance;
import osrs.merch.entry.edit.EditEntry;
import osrs.merch.entry.table.CalculationAbstractTable;
import osrs.merch.entry.table.EntryAbstractTable;

import java.util.Map.Entry;

public class Gui extends JFrame {

	private static final long serialVersionUID = -1050201775469614489L;

	private JPanel contentPane;
	public JTextField textField;
	public JTextField textField_1;
	public JTextField textField_2;
	public JTable table;
	public JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;

	public EntryAbstractTable entryTable;
	public CalculationAbstractTable calculationTable;
	public ArrayList<EntryInstance> entries;

	private int rowAtPoint;

	/**
	 * Create the frame.
	 * 
	 * @throws IOException
	 * @throws JsonIOException
	 * @throws JsonSyntaxException
	 */
	public Gui() throws JsonSyntaxException, JsonIOException, IOException {
		setTitle("RiiPiiNFtW's Merch Tool | Pre Alpha v.01");
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		ArrayList<EntryInstance> _entries = gson.fromJson(Files.newBufferedReader(Paths.get("data", "items.json")), new TypeToken<List<EntryInstance>>(){}.getType());
		this.entries = _entries;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 591, 412);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Calculate Profit Margin", null, panel, "Calculate your potential profit margin here.");
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Buying Price:");
		lblNewLabel.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lblNewLabel.setBounds(94, 11, 87, 24);
		panel.add(lblNewLabel);

		textField = new JTextField();
		textField.setFont(new Font("Baskerville Old Face", Font.PLAIN, 12));
		textField.setBounds(88, 46, 93, 20);
		panel.add(textField);
		textField.setColumns(10);

		JLabel lblSellingPrice = new JLabel("Selling Price:");
		lblSellingPrice.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lblSellingPrice.setBounds(94, 83, 87, 14);
		panel.add(lblSellingPrice);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(88, 112, 93, 20);
		textField_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 12));
		panel.add(textField_1);

		JLabel lblCalculatedMargin = new JLabel("Calculated Margin");
		lblCalculatedMargin.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lblCalculatedMargin.setBounds(337, 16, 124, 19);
		panel.add(lblCalculatedMargin);

		JLabel label = new JLabel("=");
		label.setFont(new Font("Baskerville Old Face", Font.BOLD, 24));
		label.setBounds(381, 52, 46, 14);
		panel.add(label);

		JTextPane textPane_1 = new JTextPane();
		textPane_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 20));
		textPane_1.setToolTipText("This is the total potential profit you could make off your chosen item.");
		textPane_1.setEditable(false);
		textPane_1.setBounds(327, 77, 134, 24);
		panel.add(textPane_1);

		JLabel lblItemAmount = new JLabel("Item Amount:");
		lblItemAmount.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lblItemAmount.setBounds(94, 145, 87, 24);
		panel.add(lblItemAmount);

		textField_2 = new JTextField();
		textField_2.setFont(new Font("Baskerville Old Face", Font.PLAIN, 12));
		textField_2.setBounds(88, 175, 93, 20);
		panel.add(textField_2);
		textField_2.setColumns(10);

		JButton btnCalculateMargin = new JButton("Calculate Margin");
		btnCalculateMargin.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		btnCalculateMargin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (textField.toString() != null || textField.toString() != null) {
					if (!textField.getText().toString().matches("\\d+")) {
						JOptionPane.showMessageDialog(null, "Buying Price Input contains invalid characters!",
								"An Error Occured!", 1, null);
						return;
					}
					if (!textField_1.getText().toString().matches("\\d+")) {
						JOptionPane.showMessageDialog(null, "Selling Price Input contains invalid characters!",
								"An Error Occured!", 1, null);
						return;
					}
				}

				int buyInput = Integer.parseInt(textField.getText().toString());
				int sellInput = Integer.parseInt(textField_1.getText().toString());
				int amountInput = textField_2.getText().toString().equals("") ? 1
						: Integer.parseInt(textField_2.getText().toString());

				if (amountInput > 1) {
					textPane_1.setText(String.valueOf(
							NumberFormat.getInstance().format(Math.subtractExact(buyInput, sellInput) * amountInput)));
				} else {
					textPane_1.setText(
							String.valueOf(NumberFormat.getInstance().format(Math.subtractExact(buyInput, sellInput))));
				}

			}

		});
		btnCalculateMargin.setBounds(61, 231, 158, 30);
		panel.add(btnCalculateMargin);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Log Entry", null, panel_1,
				"Input your merching/flipping transactions here to track it in the table.");
		panel_1.setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("Item Name:");
		lblNewLabel_1.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(62, 11, 103, 18);
		panel_1.add(lblNewLabel_1);

		textField_3 = new JTextField();
		textField_3.setHorizontalAlignment(SwingConstants.CENTER);
		textField_3.setFont(new Font("Baskerville Old Face", Font.PLAIN, 12));
		textField_3.setBounds(48, 32, 117, 20);
		panel_1.add(textField_3);
		textField_3.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Bought For:");
		lblNewLabel_2.setToolTipText("Not the same as the Profit Margin Tab!");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lblNewLabel_2.setBounds(62, 63, 103, 18);
		panel_1.add(lblNewLabel_2);

		textField_4 = new JTextField();
		textField_4.setHorizontalAlignment(SwingConstants.CENTER);
		textField_4.setFont(new Font("Baskerville Old Face", Font.PLAIN, 12));
		textField_4.setBounds(48, 85, 117, 20);
		panel_1.add(textField_4);
		textField_4.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("Sold For:");
		lblNewLabel_3.setToolTipText("Not the same as the Sold For price in the Profit Margin tab!");
		lblNewLabel_3.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3.setBounds(55, 117, 110, 18);
		panel_1.add(lblNewLabel_3);

		textField_5 = new JTextField();
		textField_5.setHorizontalAlignment(SwingConstants.CENTER);
		textField_5.setFont(new Font("Baskerville Old Face", Font.PLAIN, 12));
		textField_5.setBounds(48, 136, 117, 20);
		panel_1.add(textField_5);
		textField_5.setColumns(10);

		JLabel lblNewLabel_4 = new JLabel("Amount Sold:");
		lblNewLabel_4.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_4.setBounds(65, 167, 100, 14);
		panel_1.add(lblNewLabel_4);

		textField_6 = new JTextField();
		textField_6.setHorizontalAlignment(SwingConstants.CENTER);
		textField_6.setFont(new Font("Baskerville Old Face", Font.PLAIN, 12));
		textField_6.setBounds(48, 186, 117, 20);
		panel_1.add(textField_6);
		textField_6.setColumns(10);

		JButton btnNewButton_1 = new JButton("Calculate and Input Entry");
		btnNewButton_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		btnNewButton_1.setBounds(10, 217, 195, 23);
		panel_1.add(btnNewButton_1);

		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Profit Log", null, panel_2,
				"View all of your inputted transactions and track overall profit made/lost.");
		panel_2.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(0, 0, 560, 263);
		panel_2.add(scrollPane);

		table = new JTable();
		table.setFont(new Font("Baskerville Old Face", Font.PLAIN, 11));
		table.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		entryTable = new EntryAbstractTable(this.entries);
		table = new JTable(entryTable);
		table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

		final JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem editItem = new JMenuItem("Edit Entry");
		JMenuItem deleteItem = new JMenuItem("Delete Entry");

		popupMenu.add(editItem);
		popupMenu.add(deleteItem);

		popupMenu.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						rowAtPoint = table
								.rowAtPoint(SwingUtilities.convertPoint(popupMenu, new Point(0, 0), table));
						if (rowAtPoint > -1) {
							table.setRowSelectionInterval(rowAtPoint, rowAtPoint);
						}
					}
				});
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {

			}
		});

		editItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							Object itemName = entryTable.getValueAt(table.getSelectedRow(), 0);
							Object boughtAmount = entryTable.getValueAt(table.getSelectedRow(), 1);
							Object soldAmount = entryTable.getValueAt(table.getSelectedRow(), 2);
							Object itemAmount = entryTable.getValueAt(table.getSelectedRow(), 3);
							EditEntry edit = new EditEntry(entryTable, table, Gui.this);
							edit.textField.setText(itemName.toString());
							edit.textField_1.setText(Gui.unformatNumber(boughtAmount.toString()));
							edit.textField_2.setText(Gui.unformatNumber(soldAmount.toString()));
							edit.textField_3.setText(Gui.unformatNumber(itemAmount.toString()));

							edit.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
							edit.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}

		});

		deleteItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (rowAtPoint > -1) {
					entries.remove(rowAtPoint);
				}

					Gui.this.saveAll();

			}
		});
		
		table.setComponentPopupMenu(popupMenu);
		table.setModel(entryTable);

		table.getColumnModel().getColumn(0).setPreferredWidth(85);
		table.getColumnModel().getColumn(3).setPreferredWidth(62);
		table.getColumnModel().getColumn(4).setPreferredWidth(122);
		scrollPane.setViewportView(table);

		btnNewButton_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					Gui.this.entries.add(new EntryInstance(textField_3.getText().toString(),
							Integer.parseInt(textField_4.getText().toString()),
							textField_5.getText().toString().equals("") ? 0
									: Integer.parseInt(textField_5.getText().toString()),
							Integer.parseInt(textField_6.getText().toString())));
					Gui.this.entryTable.fireTableDataChanged();
					Gui.this.calculationTable.fireTableDataChanged();
					save(Gui.this);
					textField_3.setText("");
					textField_4.setText("");
					textField_5.setText("");
					textField_6.setText("");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		JButton btnNewButton = new JButton("Refresh");
		btnNewButton.setFont(new Font("Baskerville Old Face", Font.PLAIN, 12));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Gui.this.entryTable.fireTableDataChanged();
				Gui.this.calculationTable.fireTableDataChanged();
			}
		});
		btnNewButton.setBounds(190, 316, 97, 23);
		panel_2.add(btnNewButton);

		JButton btnNewButton_2 = new JButton("Clear Table");
		btnNewButton_2.setFont(new Font("Baskerville Old Face", Font.PLAIN, 12));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Gui.this.entries.clear();
				Gui.this.entryTable.fireTableDataChanged();
				Gui.this.calculationTable.fireTableDataChanged();
				try {
					save(Gui.this);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		btnNewButton_2.setBounds(290, 316, 97, 23);
		panel_2.add(btnNewButton_2);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane_1.setBounds(0, 261, 560, 51);
		panel_2.add(scrollPane_1);
		calculationTable = new CalculationAbstractTable(this.entries);
		JTable table_1 = new JTable(calculationTable);

		table_1.setModel(calculationTable);

		table_1.getColumnModel().getColumn(0).setPreferredWidth(99);
		table_1.getColumnModel().getColumn(1).setPreferredWidth(120);
		table_1.getColumnModel().getColumn(2).setPreferredWidth(127);
		table_1.getColumnModel().getColumn(3).setPreferredWidth(93);
		table_1.getColumnModel().getColumn(4).setPreferredWidth(81);
		table_1.setRowHeight(22);
		scrollPane_1.setViewportView(table_1);

	}
	
	public void saveAll() {
		try {
			this.save(this);
			this.reload(this);
			entryTable.fireTableDataChanged();
			calculationTable.fireTableDataChanged();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void save(Gui gui) throws IOException {
		Gson gson = new Gson();
		Files.write(Paths.get("data", "items.json"), gson.toJson(gui.entries).getBytes(StandardCharsets.UTF_8),
				StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
	}
	
	public void reload(Gui gui) throws IOException {
		Gson gson = new Gson();
		ArrayList<EntryInstance> entries = gson.fromJson(Files.newBufferedReader(Paths.get("data", "items.json")), new TypeToken<List<EntryInstance>>(){}.getType());
		gui.entries.clear();
		gui.entries.addAll(entries);
	}

	public static String formatNumber(double number) {
		return NumberFormat.getInstance().format(number);
	}

	public static String unformatNumber(String number) {
		return number.replaceAll(",","");
	}

}
